var gulp = require("gulp");
var browserify = require("browserify");
var ts = require('gulp-typescript');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify');
var source = require('vinyl-source-stream');
var paths = {
    pages: ['src/*.html']
};

gulp.task("copy-html", function () {
    return gulp.src(paths.pages)
        .pipe(gulp.dest("dist"));
});

gulp.task("compile", ["copy-html"], function () {

    var tsProject = ts.createProject("tsconfig.json");
    return tsProject.src()
        .pipe(ts(tsProject)).js
        .pipe(gulp.dest("dist"));
});

gulp.task("default", ["compile"], function () {
    browserify({
        debug: true,
        entries: ['dist/main.js'],
        cache: {},
        packageCache: {}
    })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest("dist"));

    // gulp.src('dist/bundle.js')
    //     .pipe(uglify())
    //     .pipe(gulp.dest('dist'));

    return gulp.src('dist/bundle.js')
        .pipe(minify())
        .pipe(gulp.dest('dist'));

});
